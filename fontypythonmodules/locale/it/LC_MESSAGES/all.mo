��    ^           �      �  $   �          :     M     a  2   y     �     �     �     �     �  
   �     �  .   	  :   ?	  "   z	  ?   �	     �	     �	     
     !
  *   1
     \
     n
     ~
     �
  +   �
     �
  #   �
          *  �   A     �  '   �     �            #   8     \  �   e  e   3     �     �  ;   �               *     @     R     c  0   k  n   �  �     	   �     �  2   �     /     I  $   W     |     �     �     �  -   �     �      �        5   -     c  !   �     �  s   �  w      &   �  0   �     �        3     H   @     �  I   �     �  +        4     Q  A   h     �     �     �  8   �     (  :   0     k  �  �  '   $     L     f     v     �  <   �     �     �                   
   A     L  5   _  C   �  #   �  3   �     1  "   =      `     �  ;   �     �     �  %   �       (   ,     U  1   n     �     �  v   �     E  @   L     �     �  #   �  .   �  	     �     �   �     n  "   �  6   �     �     �  5        :     J  	   ]  A   g  �   �  %  8     ^      k   9   �      �   
   �   +   �      	!     !     4!  "   B!  %   e!     �!  +   �!     �!  ;   �!  "   "  &   5"     \"  �   i"  �   #  )   �#  6   �#     $     *$  8   ;$  U   t$     �$  [   �$     ?%  #   U%     y%     �%  M   �%  (   �%     #&     8&  R   L&  
   �&  @   �&     �&     3   1       V                     5   T   D      S   6   N       (       #      U       %      ]   ,      /   8   -       F   +   )                 I          >   7                 \                   ?       @   <              B   !   O      J      0   $   &   Q   H   "           =   K   
             A   Z   [   ^               ;   P          M      W       R      9   X   G   Y   C       L           4              '   .       	                    *   2   E       :       
(Also check your file permissions.)  * indicates installed pogs %s already exists. %s has been purged. %s has not been purged. %s takes two arguments: SOURCE(folder) TARGET(pog) &About &Clear ENTIRE selection &Exit &Help &Select ALL the source fonts &Selection &Settings	Ctrl+S (%s) cannot be found. Try -l to see the names. (%s) skipped. I can't display this name under your locale. (%s) skipped. It's an invalid pog. A Pog with no name won't be created, however it was a good try! Are you sure? Bad voodoo error. I give up. Cannot delete the Pog.%s Change settings Checking fonts, this could take some time. Choose some fonts Clear selection Clear the selection completely. Close the app Copying fonts from %(source)s to %(target)s Could not open (%s). Could not write to the config file. Creates a new, empty Pog Creating a new pog: %s Do you want to purge %s?

Purging means all the fonts in the pog
that are not pointing to actual files
will be removed from this pog. Error Font may be bad and it cannot be drawn. Fonty Python Fonty Python version %s Fonty Python, um ... crashed. Fonty Python: bring out your fonts! H&elp	F1 I am sorry, but Unicode is not supported by this installation of wxPython. Fonty Python relies on Unicode and will simply not work without it.

Please fetch and install the Unicode version of python-wxgtk. I can't decode your argument(s). Please check your LANG variable. Also, don't paste text, type it in. I can't find %s I could not find any bad fonts. I have placed %(count)s fonts from %(folder)s into %(pog)s. Include sub-folders. Installing (%s) Jump the lazy dog fox Listing %d pog(s) Looking in %s... New Pog No config file found, creating it with defaults. Not a single font in this pog could be installed.
The original font folder has probably moved or been renamed. Not a single font in this pog could be uninstalled.
None of the fonts were in your fonts folder, please check your home .fonts (with a dot in front) folder for broken links.
The pog has been marked as "not installed". Oh boy... Page length: Pog cannot be written to.
Check your filesystem.%s Pog is already installed. Pog is empty. Pog is invalid, please hand-edit it. Pog is not installed. Point size: Purge font? Remove %s, are you sure? Remove all ghost fonts from the selected Pog. Removing (%s) SORRY: UNICODE MUST BE SUPPORTED Sample text: Select ABSOLUTELY ALL the fonts in the chosen source. Selected fonts are now in %s. Selected fonts have been removed. Settings Some fonts could not be uninstalled.
Please check your home .fonts (with a dot in front) folder for broken links.%s Some fonts did not install.
Perhaps the original fonts folder has moved or been renamed.
You should purge or hand-edit. Sorry, (%s) does not exist. Try --list Sorry, can't find (%s). Try -l to see the names. Starting in %s: Target Pogs The fonts from %(folder)s are *already* in %(pog)s. The fontypython config file is damaged.
Please remove it and start again The process is complete. The target pog (%s) is currently installed, you can't use it as a target. There are no fonts in here. There are no fonts to see here, move along. There are no pogs available. There is no such item. There was an error writing the pog to disk. Nothing has been done This folder has no fonts in it. This font is in %s This pog is empty Unicode problem. Font may be bad and it cannot be drawn. Warning You cannot use a folder as the target argument. Try --help Your pogs are the same! Try -e Project-Id-Version: Fontypython 0.3.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-06 20:46+0200
PO-Revision-Date: 2009-07-07 12:35+0100
Last-Translator: Pietro Battiston <toobaz@email.it>
Language-Team: Italian Translation Project <tp@lists.linux.it>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
 
(Controlla anche i permessi dei file.) * indica i pog installati %s esiste già. %s è stato epurato. %s non è stato epurato. %s richiede due argomenti: SORGENTE(cartella) OBIETTIVO(pog) I&nformazioni su... &Ripulisci TUTTA la selezione &Esci A&iuto &Seleziona TUTTI i font sorgente &Selezione Preferen&ze	Ctrl+S Impossibile trovare (%s). Prova -l per vedere i nomi. (%s) skipped. Non riesco a mostrarne il nome con la locale attuale. (%s) saltato. È un pog non valido. Buon tentativo... ma non creerò un pog senza nome! Sei sicuro? Errore voodoo maligno. Mi arrendo. Impossibile cancellare il pog.%s Cambia le preferenze Sto controllando i font: potrebbe prendere un po' di tempo. Scegli i font Deseleziona tutto Ripulisci completamente la selezione. Chiudi il programma Copia di font da %(source)s a %(target)s Impossibile aprire (%s). Impossibile modificare il file di configurazione. Crea un nuovo pog vuoto Creo un nuovo pog: %s Vuoi epurare %s?

Epurare significa che tutti i font del
pog che non puntano a file effettivi
saranno rimossi dal pog. Errore Il font potrebbe essere corrotto, non è possibile raffigurarlo. Fonty Python Fonty Python, versione %s Fonty Python... ehm... ha crashato. Fonty Python: porta i tuoi font allo scoperto! A&iuto	F1 Spiacente, questa installazione di wxpython non supporta l'Unicode. Fonty Python necessità di Unicode e semplicemente non può funzionare senza.

Per favore procurati ed installa la versione Unicode di python-wxgtk. Impossibile decifrare il/gli argomento/i. Verificare la variabile LANG. Attenzione: non incollare testo, ma digitarlo direttamente. Impossibile trovare %s Non ho trovato alcun font dannoso. Ho sistemato %(count)s fonts da %(folder)s in %(pog)s. Includi sottocartelle. Installo (%s) Ma la volpe col suo balzo ha raggiunto il quieto fido Lista di %d pog Controllo in %s... Nuovo pog Non ho trovato il file di configurazione, ne ricreo uno standard. Non è stato possibile installare neanche un font di questo pog.
La cartella di origine dei font è stata probabilmente spostata o rinominata. Non è stato possibile disinstallare neanche un font di questo pog.
Nessuno di questi font era nella tua cartella dei font, per favore controlla eventuali link interrotti nella cartella .fonts (con un punto all'inizio) all'interno della tua home.
Il pog è stato segnato come "non installato". O cribbio... Lunghezza della pagina: Impossibile modificare il pog.
Controlla il filesystem.%s Pog già installato. Pog vuoto. Il pog è invalido, modificalo manualmente. Pog non installato. Dimensione (in punti): Epurare font? Sei sicuro di voler cancellare %s? Rimuovi fantasmi dal pog selezionato. Rimuovo (%s) SPIACENTE: L'UNICODE DEVE ESSERE SUPPORTATO Testo di esempio: Seleziona ASSOLUTAMENTE TUTTI i font nella sorgente scelta. I font selezionati sono ora in %s. I font selezionati sono stati rimossi. Impostazioni Impossibile disinstallare alcuni font.
Per favore controlla eventuali link interrotti nella cartella .fonts (con un punto all'inizio) all'interno della tua home.%s Non è stato possibile installare alcuni font.
Probabilmente la cartella di origine dei font è stata rimossa o rinominata.
Doversti epurarli o modificare manualmente il pog. Spiacente, (%s) non esiste. Prova --list. Spiacente, non trovo (%s). Prova -l per vedere i nomi. Parto in %s: Pog destinazione I font della cartella %(folder)s sono *già* in %(pog)s. Il file di configurazione fontypython è danneggiato.
Per favore rimuovilo e riprova. Il processo è completo. Il pog destinazione (%s) è attualmente installato, non puoi utilizzarlo come destinazione. Non ci sono font qui. Qui non c'è nessun font da vedere. Nessun pog disponibile. Non c'è un tale elemento. C'è stato un errore nella memorizzazione del file. Non è stato fatto nulla. Questa cartella non contiene alcun font. Questo font è in %s Questo pog è vuoto Problema unicode. Il font potrebbe essere corrotto, non è possibile raffigurarlo. Attenzione Non puoi utilizzare una cartella come destinazione. Prova --help I pog sono lo stesso! Prova -e 