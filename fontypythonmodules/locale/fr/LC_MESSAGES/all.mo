��    <      �  S   �      (  $   )     N     j     }     �     �     �     �  .   �     �          !     :     J  +   X     �  �   �          %  #   2     V     _     o     �     �  0   �  n   �  �   ?     	  2   &	     Y	     s	  $   �	     �	     �	     �	     �	     �	     �	     
  !   &
     H
  s   Q
  w   �
  &   =  0   d     �  H   �  I   �     4  +   P     |     �  A   �     �          $  :   ,     g  |  �  (        ,     K     a     |  
   �     �     �  F   �     �  &     !   ;     ]     s  /   �     �  �   �     d     k  8   x     �     �  -   �     �       U   "  n   x  �   �     �  E   �     +     >  3   O     �     �     �  ,   �     �       4     2   R     �  }   �  �     F   �  F   �     @  l   L  V   �           (  !   I  !   k  X   �  &   �       	     I   (  %   r                %      &   $      2                  3              /   ;   (      .                 5           '                     <   9   )                           -         8          :   4                1   !      0       *          ,   
              7         "   #   +   6         	           
(Also check your file permissions.)  * indicates installed pogs %s already exists. %s has been purged. %s has not been purged. &About &Help &Settings	Ctrl+S (%s) cannot be found. Try -l to see the names. Are you sure? Bad voodoo error. I give up. Cannot delete the Pog.%s Clear selection Close the app Copying fonts from %(source)s to %(target)s Could not open (%s). Do you want to purge %s?

Purging means all the fonts in the pog
that are not pointing to actual files
will be removed from this pog. Error Fonty Python Fonty Python: bring out your fonts! H&elp	F1 Installing (%s) Jump the lazy dog fox Listing %d pog(s) New Pog No config file found, creating it with defaults. Not a single font in this pog could be installed.
The original font folder has probably moved or been renamed. Not a single font in this pog could be uninstalled.
None of the fonts were in your fonts folder, please check your home .fonts (with a dot in front) folder for broken links.
The pog has been marked as "not installed". Page length: Pog cannot be written to.
Check your filesystem.%s Pog is already installed. Pog is empty. Pog is invalid, please hand-edit it. Pog is not installed. Point size: Purge font? Remove %s, are you sure? Removing (%s) Sample text: Selected fonts are now in %s. Selected fonts have been removed. Settings Some fonts could not be uninstalled.
Please check your home .fonts (with a dot in front) folder for broken links.%s Some fonts did not install.
Perhaps the original fonts folder has moved or been renamed.
You should purge or hand-edit. Sorry, (%s) does not exist. Try --list Sorry, can't find (%s). Try -l to see the names. Target Pogs The fontypython config file is damaged.
Please remove it and start again The target pog (%s) is currently installed, you can't use it as a target. There are no fonts in here. There are no fonts to see here, move along. There are no pogs available. There is no such item. There was an error writing the pog to disk. Nothing has been done This folder has no fonts in it. This pog is empty Warning You cannot use a folder as the target argument. Try --help Your pogs are the same! Try -e Project-Id-Version: fontypython 0.2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-06 20:46+0200
PO-Revision-Date: 2008-01-22 19:46+0200
Last-Translator: Donn Ingle <donn.ingle@gmail.com>
Language-Team: French <traduc@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
(Vérifiez également les permissions.)  * indique les pogs installés Pog %s est installé. %s n'a pas été nettoyé. %s n'a pas été nettoyé. À &Propos &Aide &Options	Ctrl+O (%s) ne peut être trouvé. essayez -l pour voir les noms disponibles. Êtes-vous certain ? Erreur maléfique vaudou, j'abandonne. Impossible de supprimer le Pog.%s Ne rien sélectionner Quitter Fonty Python Copie des polices de %(source)s vers %(target)s Impossible d'ouvrir (%s). Voulez-vous nettoyer %s ?

Nettoyer signifie que toutes les polices du Pog qui
ne pointent pas vers des fichiers existants en
seront supprimées. Erreur Fonty Python Fonty Python : redécouvrez vos polices de caractères ! &Aide	F1 Installer le Pog (%s) Portez ce vieux whisky au juge blond qui boit Affichage de %d pog(s)  Nouveau Pog Aucun fichier de configuration détecté, création avec les paramètres par défaut. Pas la moindre police de ce pog n'a pu être installée.
Le dossier originel a du être supprimé ou renommé. Pas la moindre police de ce pog n'a pu être désinstallée.
Aucune d'entre elles n'étaient dans votre dossier de polices, veuillez
vérifiez le dossier ~/.fonts/ (sans oublier le point).
Le pog a été marqué comme désinstallé. Longueur de la page: Le pog ne peut être enregistré.
Vérifiez le système de fichier.%s Pog est installé. Le pog est vide. Le pog est invalide, veuillez l'éditer à la main. Le Pog n'est pas installé. Longueur de la page: Supprimer la police ? Êtes-vous certain de vouloir supprimer %s ? Visualisation de (%s) Texte affiché: Les polices sélectionnées sont maintenant dans %s. Les polices sélectionnées ont été supprimées. Options Certaines polices ne peuvent pas être désinstallées.
Veuillez vérifier votre dossier ~/.fonts/ (sans oublier le point).%s Certaines polices n'ont pas été installées.
Peut-être que le dossier d'origine a été déplacé ou renommé.
Vous devriez le purger (-p) ou l'éditer à la main. (%s) ne peut être trouvé. essayez -l pour voir les noms disponibles. (%s) ne peut être trouvé. essayez -l pour voir les noms disponibles. Pogs Cibles Le fichier ~/.fontypython/fp.conf semble être endommagé.
Veuillez le supprimer puis relancer Fonty Python. Le pog (%s) est actuellement installé, vous ne pouvez pas l'utiliser comme une cible. Aucun fonts disponible. Choisissez un dossier ou un Pog, Il n'y a pas autant d'éléments. Il n'y a pas autant d'éléments. Il y a eu une erreur pendant l'écriture du pog sur le disque. Rien n'a été effectué. Ce dossier ne contient pas de fichier. Le pog est vide. Attention Vous ne pouvez pas utiliser un dossier comme un pog cible. Essayez --help Vos pogs sont les mêmes ! Essayez -e 