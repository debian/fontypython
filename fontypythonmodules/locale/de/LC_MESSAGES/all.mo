��    b      ,  �   <      H  $   I     n     �     �     �     �  2   �     	     	     4	     :	     @	  
   ]	     h	  .   y	  :   �	  "   �	  ?   
     F
     T
     q
     �
  *   �
     �
     �
     �
       +        A  #   V     z     �  �   �     0  +   6  '   b     �     �     �  #   �     �  �   �  e   �     .     >     Z  ;   z     �     �     �     �            0     n   M  �   �  	   �     �  2   �     �     �  $        -     C     O     [  -   t     �      �     �  5   �       !   2     T  s   ]  w   �  &   I  0   p     �     �  3   �  H   �     :  I   S     �  +   �     �       A     B   [     �     �     �  8   �       :   $     _  B  ~  )   �  "   �          $     8     R  1   n     �     �  
   �     �  (   �     �       H     ^   _  -   �  I   �     6     G  %   f     �  0   �     �     �             "   5     X  /   q     �     �  �   �     q  D   x  D   �            #   '  .   K  	   z  �   �  �   a     �       9   "  8   \      �     �  7   �     �        	   "   G   ,   �   t     �      "     &"  N   4"     �"     �"  8   �"     �"     #     #  "   $#  7   G#     #  5   �#     �#  8   �#  /   
$  $   :$     _$  �   h$  �   2%  -   �%  M    &     N&     ]&  8   m&  _   �&     '  V   '  #   s'  0   �'     �'     �'  H   �'  H   A(  2   �(  !   �(     �(  U   �(     G)  I   O)  +   �)     >   L             7   T   M   9   1            `         -   /   J   b               \       0   
      	                   G       K      <   R   O      Q       4       B         H   :   C      %      )   +   P   [   a       D   U   ]   "   ?   (   N   2       Y   Z   =   _       E   .                  #       F   A       V   8   !   ,   S   3   6                     $   *       I   X              ;   '             ^              W   5           &                                @        
(Also check your file permissions.)  * indicates installed pogs %s already exists. %s has been purged. %s has not been purged. %s is already installed. %s takes two arguments: SOURCE(folder) TARGET(pog) &About &Clear ENTIRE selection &Exit &Help &Select ALL the source fonts &Selection &Settings	Ctrl+S (%s) cannot be found. Try -l to see the names. (%s) skipped. I can't display this name under your locale. (%s) skipped. It's an invalid pog. A Pog with no name won't be created, however it was a good try! Are you sure? Bad voodoo error. I give up. Cannot delete the Pog.%s Change settings Checking fonts, this could take some time. Choose some fonts Clear selection Clear the selection completely. Close the app Copying fonts from %(source)s to %(target)s Could not open (%s). Could not write to the config file. Creates a new, empty Pog Creating a new pog: %s Do you want to purge %s?

Purging means all the fonts in the pog
that are not pointing to actual files
will be removed from this pog. Error Font causes a segfault. It cannot be drawn. Font may be bad and it cannot be drawn. Fonty Python Fonty Python version %s Fonty Python, um ... crashed. Fonty Python: bring out your fonts! H&elp	F1 I am sorry, but Unicode is not supported by this installation of wxPython. Fonty Python relies on Unicode and will simply not work without it.

Please fetch and install the Unicode version of python-wxgtk. I can't decode your argument(s). Please check your LANG variable. Also, don't paste text, type it in. I can't find %s I can't find a pog named %s I could not find any bad fonts. I have placed %(count)s fonts from %(folder)s into %(pog)s. Include sub-folders. Installing (%s) Jump the lazy dog fox Listing %d pog(s) Looking in %s... New Pog No config file found, creating it with defaults. Not a single font in this pog could be installed.
The original font folder has probably moved or been renamed. Not a single font in this pog could be uninstalled.
None of the fonts were in your fonts folder, please check your home .fonts (with a dot in front) folder for broken links.
The pog has been marked as "not installed". Oh boy... Page length: Pog cannot be written to.
Check your filesystem.%s Pog is already installed. Pog is empty. Pog is invalid, please hand-edit it. Pog is not installed. Point size: Purge font? Remove %s, are you sure? Remove all ghost fonts from the selected Pog. Removing (%s) SORRY: UNICODE MUST BE SUPPORTED Sample text: Select ABSOLUTELY ALL the fonts in the chosen source. Selected fonts are now in %s. Selected fonts have been removed. Settings Some fonts could not be uninstalled.
Please check your home .fonts (with a dot in front) folder for broken links.%s Some fonts did not install.
Perhaps the original fonts folder has moved or been renamed.
You should purge or hand-edit. Sorry, (%s) does not exist. Try --list Sorry, can't find (%s). Try -l to see the names. Starting in %s: Target Pogs The fonts from %(folder)s are *already* in %(pog)s. The fontypython config file is damaged.
Please remove it and start again The process is complete. The target pog (%s) is currently installed, you can't use it as a target. There are no fonts in here. There are no fonts to see here, move along. There are no pogs available. There is no such item. There was an error writing the pog to disk. Nothing has been done There was an error writing the pog to disk. Nothing has been done. This folder has no fonts in it. This font is in %s This pog is empty Unicode problem. Font may be bad and it cannot be drawn. Warning You cannot use a folder as the target argument. Try --help Your pogs are the same! Try -e Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-06 20:46+0200
PO-Revision-Date: 2009-09-28 10:11+0200
Last-Translator: Donn <donn.ingle@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
(Auch die Nutzerberechtigungen prüfen.)  (* kennzeichnet installiert Pogs) %s existiert bereits. %s wurde gelöscht. %s wurde nicht gelöscht. %s ist bereits installiert. %s nimmt zwei Argumente: QUELLE(Ordner) ZIEL(Pog) Ü&ber &GESAMTE Auswahl aufheben &Verlassen &Hilfe &ALLE Schriften im Ordner/Pog auswählen &Auswahl &Optionen	Ctrl+O "%s" nicht gefunden. Versuchen Sie -l zum Anzeigen aller Namen der Pogs. "%s" wurde übersprungen, Name kann mit der eingestellten Lokalisation nicht angezeigt werden. "%s" wurde übersprungen, kein gültiges Pog. Ein Pog ohne Namen wird nicht angelegt, aber immerhin ein netter Versuch! Sind Sie sicher? Schwerer Fehler, ich gebe auf. Pog konnte nicht gelöscht werden. %s Einstellungen verändern Prüfe die Schriften, einen Moment Geduld bitte. Wähle einige Schriften Nichts &auswählen Hebt die Auswahl komplett auf. Fonty Python beenden Kopiere %(source)s nach %(target)s Kann "%s" nicht öffnen. Konnte die Konfigurationsdatei nicht schreiben. Erstellt ein neues, leeres Pog. Erstellt ein neues Pog: %s %s soll bereinigt werden?

Bereinigen bedeutet, dass alle Schriften in diesem Pog,
die nicht auf eine vorhandene Schrift verweisen, entfernt werden. Fehler Schrift könnte beschädigt sein und nicht richtig angezeigt werden. Schrift könnte beschädigt sein und nicht richtig angezeigt werden. Fonty Python Fonty Python Version %s Fonty Python, .... ist abgestürzt. Fonty Python : hole mehr aus Deinen Schriften! &Hilfe	F1 Entschuldigung, aber installierte Version von wxPython unterstützt Unicode nicht. Fonty Python ist aber auf Unicode angewiesen und wird ohne nicht arbeiten.

Bitte besorgen Sie sich die Unicode-Version von python-wxgtk. Ich kann die Argumente nicht dekodieren. Bitte überprüfen Sie die Umgebungsvariable LANG. Kopieren Sie nicht den Text, sondern geben ihn direkt ein. Kann %s nicht finden Kann %s nicht finden Es konnten keine beschädigten Schriften gefunden werden. %(count)s Schriften aus %(folder)s nach %(pog)s kopiert. Unterverzeichnisse einschliessen Installiere "%s" Der rötliche Vogel springt über das bläuliche Faß.  Liste %d Pog(s) auf Suche in %s... Neues Pog Keine Konfigurationsdatei gefunden, wird mit Standardvorgaben erstellt. Keine einzige Schrift aus diesem Pog konnte installiert werden.
Möglicherweise wurden die Original-Schriften verschoben oder umbenannt. Keine einzige Schrift aus diesem Pog konnte deinstalliert werden.
Keine davon befand sich in ihrem Heimatverzeichnis.
Schauen Sie unter ~/.fonts/ (WICHTIG: versteckter Ordner mit Punkt als erstes Zeichen)
nach verwaisten Verknüpfungen.
Das Pog wurde als "nicht installiert" markiert. Oh Junge... Seitenlänge: Es konnte nicht in das Pog geschrieben werden.
Prüfen Sie das Dateisystem. %s Pog ist bereits installiert. Pog ist leer. Pog ist nicht gültig, bearbeiten Sie es bitte von Hand. Pog ist nicht installiert. Schriftgröße: Schrift löschen? Soll %s wirklich gelöscht werden? Alle verwaisten Schriften vom gewählten Pog entfernen. Entferne "%s" ENTSCHULDIGUNG, ABER UNICODE MUSS UNTERSTÜTZT WERDEN Beispieltext: Wählt RESTLOS ALLE Schriften im Ursprungsordner/Pog aus Ausgewählte Schriften befindne sich nun in %s. Gewählte Schriften wurden entfernt. Optionen Einige Schriften konnten nicht deinstalliert werden.
Schauen Sie in Ihrem Heimatverzeichnis unter
~/.fonts/ (WICHTIG: versteckter Ordner mit Punkt als erstes Zeichen)
nach verwaisten Verknüpfungen. %s Einige Schriften wurden nicht installiert.
Vielleicht wurden die Original-Schriften verschoben oder umbenannt.
Entweder löschen (-p) oder von Hand bearbeiten. "%s" existiert nicht. Versuche -l oder --list Entschuldigung,"%s" nicht gefunden. Versuche -l für die Anzeige aller Namen. Beginne in %s: Vorhandene Pogs Die Schriften aus %(folder)s sind  *bereits* in %(pog)s. Die Datei ~/.fontypython/fp.conf enthält Fehler.
Bitte entfernen und Fonty Python neu starten. Vorgang abgeschlossen Das Pog "%s" ist im Moment installiert und kann daher nicht als Ziel verwendet werden. Hier befinden sich keine Schriften. Hier befinden sich keine Schriften, gehe weiter. Keine Pogs verfügbar. Element nicht vorhanden. Ein Fehler trat beim Schreiben des Pogs auf. Es wurde nichts verändert. Ein Fehler trat beim Schreiben des Pogs auf. Es wurde nichts verändert. Es befinden sich keine Schriften in diesem Ordner. Diese Schrift befindet sich in %s Das Pog ist leer. Unicode Problem. Schrift könnte beschädigt sein und nicht richtig angezeigt werden. Achtung Ein Ordner kann nicht als Ziel-Argument verwendet werden. Versuche --help Quell- und Zielpog sind gleich! Versuche -e 