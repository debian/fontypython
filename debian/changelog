fontypython (0.5-2) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 4.2.1
    + Removed x-python-version field.
    + Fixed Vcs-* URLs.
    + Updated Build-Depends.
    + Recommends python-gi now.
  * Updated debian/copyright.
  * debian/rules:
    + Removed extra license files which were put as README.
    + Use pybuild build system.

 -- Kartik Mistry <kartik@debian.org>  Wed, 28 Nov 2018 14:03:16 +0530

fontypython (0.5-1) unstable; urgency=low

  * New upstream release.
    + Removed patches merged upstream.
    + Refreshed: 0005-Install-data-files-in-usr-share.patch
  * Added patch 0006-Desktop-keyword.patch to add Keywords entry in .desktop
    file.
  * debian/control:
    + Updated Vcs* URLs.
    + Updated Standards-Version to 4.1.3.
    + Bumped debhelper dependency to 11.
  * Bumped dh to 11.
  * Updated debian/watch to use https.
  * debian/rules:
    + Removed extra license files.
    + Some minor cleanups.
  * Added upstream signing key.

 -- Kartik Mistry <kartik@debian.org>  Mon, 15 Jan 2018 14:25:26 +0530

fontypython (0.4.6-3) unstable; urgency=low

  [ Pietro Battiston ]
  * Added patch to replace python-imaging with python-pil (Closes: #866424)
  * debian/control:
    + Depends and Build-Depends on python-pil instead of python-imaging.

  [ Kartik Mistry ]
  * debian/control:
    + Added Pietro Battiston as a co-maintainer.

 -- Kartik Mistry <kartik@debian.org>  Fri, 30 Jun 2017 20:21:44 +0530

fontypython (0.4.6-2) unstable; urgency=low

  * debian/rules:
    + Make the build reproducible, patch by Chris Lamb <lamby@debian.org>
      (Closes: #853039)
  * Updated debian/copyright years.
  * debian/control:
    + Updated Standards-Version to 4.0.0

 -- Kartik Mistry <kartik@debian.org>  Fri, 23 Jun 2017 22:12:42 +0530

fontypython (0.4.6-1) unstable; urgency=low

  * New upstream release.
  * Added upstream pgp signature support in debian/watch.

 -- Kartik Mistry <kartik@debian.org>  Sun, 10 Jul 2016 13:30:20 +0530

fontypython (0.4.5-1) unstable; urgency=low

  * New upstream release.
  * Acknowledge all NMUs.
  * Dropped following merged upstream patches:
    + 0002-Removed-unused-code-which-causes-crash-with-wx-3.0.patch
    + 0003-Adapt-to-wxpython-3.0-which-enforces-assertions-on-L.patch (Adapted)
    + 0004-Replace-obsolete-tostring-with-tobytes-Closes-820957.patch
  * debian/control:
    + Use debhelper 9.
    + Updated Standards-Version to 3.9.8
    + Fixed Vcs-* URLs.

 -- Kartik Mistry <kartik@debian.org>  Thu, 30 Jun 2016 20:08:03 +0530

fontypython (0.4.4-1.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Added Vcs-* fields
  * Patch: replace obsolete "tostring" with "tobytes" (Closes: #820957)
  * Patch: install data files in /usr/share rather than /usr/lib
  * New standards version
  * Build-Depends on dh-python
  * Removed Debian menu item

 -- Pietro Battiston <me@pietrobattiston.it>  Mon, 25 Apr 2016 09:28:04 +0200

fontypython (0.4.4-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Removed flawed patch (wx 2.8 is not in jessie)
  * Patch: do not crash on empty pogs list
  * Patches for compatibility with wxpython 3.0 (Closes: #773579)

 -- Pietro Battiston <me@pietrobattiston.it>  Sun, 21 Dec 2014 22:54:02 +0100

fontypython (0.4.4-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Patch: select wx 2.8 (rather than 3.0) (Closes: #765487)

 -- Pietro Battiston <me@pietrobattiston.it>  Sun, 14 Dec 2014 09:39:51 +0100

fontypython (0.4.4-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update to depend on python-wxgtk3.0 rather than python-wxgtk2.8.
    (Closes: #757886)

 -- Olly Betts <olly@survex.com>  Thu, 04 Sep 2014 00:18:50 +0000

fontypython (0.4.4-1) unstable; urgency=low

  * New upstream release
  * Dropped all patches, merged upstream.

 -- Kartik Mistry <kartik@debian.org>  Sun, 15 Apr 2012 13:21:32 +0530

fontypython (0.4.2.3-5) unstable; urgency=low

  * debian/patches/Fix_for_.fonts_dir_creation.diff:
    + Fixed patch from upstream, now set userfontpath to ~/.fonts directory to
      install POGS properly.

 -- Kartik Mistry <kartik@debian.org>  Sat, 14 Apr 2012 16:59:28 +0530

fontypython (0.4.2.3-4) unstable; urgency=low

  * debian/patches/Fix_for_.fonts_dir_creation.diff:
    + Added patch to not look for .fonts while building, fixes FTBFS
      (Closes: #665073)
  * debian/copyright:
    + Updated to copyright-format 1.0
  * debian/control:
    + Updated Standards-Version to 3.9.3

 -- Kartik Mistry <kartik@debian.org>  Fri, 30 Mar 2012 22:42:21 +0530

fontypython (0.4.2.3-3) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.9.2 (no changes needed).
    + Bumped debhelper dependency to >= 7.0.50~
    + Added X-Python-Version field.
  * debian/rules:
    + Use dh sequencer and dh_python2.
    + Also removes symlinked to extrea COPYING file.
  * Added debian/docs and debian/manpages files, Removed pyversions file.
  * debian/copyright:
    + Updated to DEP-5 format
  * debian/patches/typo-translation-fixes.diff:
    + Added patch to fix typo and translation in Desktop file (Closes: #620148)
      Thanks to Andrea Carpineti <carpineti.dev@gmail.com> for patch.

 -- Kartik Mistry <kartik@debian.org>  Thu, 25 Aug 2011 12:19:58 +0530

fontypython (0.4.2.3-2) unstable; urgency=low

  * debian/control:
    + Added ${misc:Depends} to Depends field
  * debian/copyright:
    + Updated Debian package copyright year
  * Converted package to new source format '3.0 (quilt)'

 -- Kartik Mistry <kartik@debian.org>  Wed, 13 Jan 2010 10:01:25 +0530

fontypython (0.4.2.3-1) unstable; urgency=low

  * New upstream release

 -- Kartik Mistry <kartik@debian.org>  Sat, 24 Oct 2009 19:22:59 +0530

fontypython (0.4.2.2-1) unstable; urgency=low

  * New upstream release
  * debian/rules:
    + Don't rename fp to fontypython, fixed upstream
    + Linked wrapper script manpage start_fontypython.1 to fontypython.1
  * fontypython.1 manpage is adopted upstream, removed it from debian/

 -- Kartik Mistry <kartik@debian.org>  Thu, 01 Oct 2009 22:59:43 +0530

fontypython (0.4.2.1-2) unstable; urgency=low

  * Added debian/pyversions since fontypython requires Python >= 2.5, Thanks
    to Aaron M. Ucko <ucko@debian.org> (Closes: #548981)

 -- Kartik Mistry <kartik@debian.org>  Wed, 30 Sep 2009 20:16:24 +0530

fontypython (0.4.2.1-1) unstable; urgency=low

  * New upstream release
  * Fixed debian/watch file for upstream tarball
  * debian/control:
    + Updated Standards-Version to 3.8.3 (no changes needed)
    + Updated Build-Depends on python >= 2.5 instead of python-all-dev
  * debian/rules:
    + Installed upstream changelog
    + Don't put remoaval of .po files in binary, fixed upstream
  * debian/menu:
    + Adopted latest way of writing it
    + Updated title with better description
  * debian/fontypython.1:
    + Added -z option, added with this new upstream version
    + Updated descriptions
    + Wrapped upto 80 characters
  * debian/copyright:
    + Don't point to versionless symlink to license text

 -- Kartik Mistry <kartik@debian.org>  Tue, 29 Sep 2009 11:16:37 +0530

fontypython (0.4.1-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Better short and long description as discussed with upstream
  * debian/rules:
    + Do not install .po files in package

 -- Kartik Mistry <kartik@debian.org>  Sat, 25 Jul 2009 11:41:24 +0530

fontypython (0.4-2) unstable; urgency=low

  * debian/control:
    + Updated dependency to wxgtk2.8, Thanks to Michael Hoeft
      <mike679@unitybox.de> (Closes: #537612)
    + Updated short and long description to better one, Thanks
      to Donn Ingle <donn.ingle@gmail.com>
  * debian/fontypython.1:
    + Updated manpage for newly added options

 -- Kartik Mistry <kartik@debian.org>  Mon, 20 Jul 2009 09:33:30 +0530

fontypython (0.4-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Updated Standards-Version to 3.8.2
    + Wraped Build-Depends and Depends for readability
    + Updated debhelper dependency to 7
    + Updated Section to fonts from utility
  * debian/watch:
    + Updated according to upstream filename changes
  * debian/rules:
    + Using dh_prep instead of dh_clean -k now
    + Removed extra LICENSE file from package
  * debian/copyright:
    + Do not use symlinked license text

 -- Kartik Mistry <kartik@debian.org>  Sat, 11 Jul 2009 12:14:51 +0530

fontypython (0.3.6-3) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.8.1
  * debian/rules:
    + Fixed FTBFS with Python 2.6, Thanks to Alessio Treglia
      <quadrispro@ubuntu.com> for report and patch (Closes: #527002)
    + Included /usr/share/python/python.mk
    + Append py_setup_install_args macro to setup.py install args list
  * debian/fontypython.1:
    + Updated my email address

 -- Kartik Mistry <kartik@debian.org>  Thu, 14 May 2009 23:54:34 +0530

fontypython (0.3.6-2) unstable; urgency=low

  * debian/control:
    + Updated my maintainer address
    + Updated Standards-Version to 3.8.0
  * debian/copyright:
    + [Lintian] Updated to use © symbol instead of (C)
    + Updated Debian packaging copyright year

 -- Kartik Mistry <kartik@debian.org>  Mon, 23 Feb 2009 22:46:21 +0530

fontypython (0.3.6-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Updated Standards-Version to 3.7.3
    + Updated Homepage entry
  * debian/copyright:
    + Updated package license to GPL-3
    + Updated upstream copyright year
  * debian/fontypython.1:
    + Updated to add new command line options
  * debian/rules:
    + Fixed clean target by removing fontypythonmodules/*.pyc files

 -- Kartik Mistry <kartik.mistry@gmail.com>  Wed, 23 Jan 2008 12:23:56 +0530

fontypython (0.2.0-3) unstable; urgency=low

  * debian/copyright: separated copyright from license
  * debian/fontypython.1: fixed manpage according to standard format, arranged
    options in logical order
  * debian/menu: fixed according to latest menu policy, used " Applications"
    instead of "Apps"
  * debian/control: fixed short and long descriptions

 -- Kartik Mistry <kartik.mistry@gmail.com>  Sat, 22 Aug 2007 12:25:42 +0530

fontypython (0.2.0-2) unstable; urgency=low

  * Added missing debian/menu file
  * debian/control: fixed build-dependency by moving it to Build-Depends-Indep
    as package is arch: all

 -- Kartik Mistry <kartik.mistry@gmail.com>  Fri, 11 May 2007 16:52:54 +0530

fontypython (0.2.0-1) unstable; urgency=low

  * Initial release (Closes: #420584)
  * Renamed binary from fp to fontypython as it was already taken by freepascal
  * Added manpage: debian/fontypython.1

 -- Kartik Mistry <kartik.mistry@gmail.com>  Mon, 23 Apr 2007 15:58:29 +0530
